package com.smartapps.interiew_demo.view;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.smartapps.interiew_demo.databinding.ActivityFavouriteFlightListBinding;
import com.smartapps.interiew_demo.util.LogUtils;
import com.smartapps.interiew_demo.view.adapter.FavFlightListAdapter;
import com.smartapps.interiew_demo.viewModel.FlightInfoViewModel;

public class FavouriteFlightListActivity extends AppCompatActivity {
    private static final String TAG = "FavouriteFlightListActivity";
    private ActivityFavouriteFlightListBinding binding;
    private FavFlightListAdapter adapter;
    private FlightInfoViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFavouriteFlightListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getSupportActionBar().setTitle("Favourite");
        initView();
        viewModel = new ViewModelProvider(this).get(FlightInfoViewModel.class);
        subscribeToDataChanges();
    }

    private void subscribeToDataChanges() {
        viewModel.getFavFlightList().observe(this, flights -> {
            if (!flights.isEmpty()) {
                binding.favFlightList.setVisibility(View.VISIBLE);
                binding.NoFavoritesSelected.setVisibility(View.GONE);
                LogUtils.d(TAG, "subscribeDataEvents: Fav Flights found :: " + flights.size());
                adapter.updateFlightList(flights);
            } else {
                LogUtils.d(TAG, "subscribeDataEvents:Fav Flights  not found ");
                binding.favFlightList.setVisibility(View.GONE);
                binding.NoFavoritesSelected.setVisibility(View.VISIBLE);
            }
        });
    }

    private void initView() {
        adapter = new FavFlightListAdapter(this);
        binding.favFlightList.setLayoutManager(new LinearLayoutManager(this));
        binding.favFlightList.setAdapter(adapter);
    }
}