package com.smartapps.interiew_demo.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.smartapps.interiew_demo.R;
import com.smartapps.interiew_demo.databinding.ItemFlightBinding;
import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;

import java.util.ArrayList;
import java.util.List;

public class FavFlightListAdapter extends RecyclerView.Adapter<FavFlightListAdapter.FlightInfoHolder> {
    private List<FavouritesFlight> flightList = new ArrayList<>();
    private Context context;

    public FavFlightListAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public FlightInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFlightBinding binding = ItemFlightBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FlightInfoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FlightInfoHolder holder, int position) {
        holder.bind(context, flightList.get(position));
    }

    @Override
    public int getItemCount() {
        return flightList.size();
    }

    public void updateFlightList(List<FavouritesFlight> flightist) {
        //ToDO need to clear data
        this.flightList.clear();
        this.flightList.addAll(flightist);
        notifyDataSetChanged();
    }

    class FlightInfoHolder extends RecyclerView.ViewHolder {
        ItemFlightBinding binding;

        public FlightInfoHolder(@NonNull ItemFlightBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.favButton.setVisibility(View.GONE);
        }

        public void bind(Context context, FavouritesFlight flight) {
            binding.missionName.setText(flight.getMission_name());
            binding.launchDate.setText(flight.getLaunch_date_local());
            binding.rocketName.setText(flight.getRocket().getRocket_name());
            binding.status.setText(flight.getLaunchStatus());
            Glide.with(context)
                    .load(flight.getLinks().getMission_patch())
                    .placeholder(R.drawable.ic_flight) // Optional placeholder image while loading
                    .error(R.drawable.ic_flight) // Optional error image if the load fails
                    .into(binding.imageFlight);
        }
    }

}
