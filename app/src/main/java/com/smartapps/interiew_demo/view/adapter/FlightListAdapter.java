package com.smartapps.interiew_demo.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.smartapps.interiew_demo.R;
import com.smartapps.interiew_demo.databinding.ItemFlightBinding;
import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;

import java.util.ArrayList;
import java.util.List;

public class FlightListAdapter extends RecyclerView.Adapter<FlightListAdapter.FlightInfoHolder> {

    private List<Flight> flightList = new ArrayList<>();
    private List<FavouritesFlight> favFlightList = new ArrayList<>();

    private Context context;
    private FlightListActionListener listener;

    public FlightListAdapter(Context context, FlightListActionListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public FlightInfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFlightBinding binding = ItemFlightBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FlightInfoHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull FlightInfoHolder holder, int position) {
        holder.bind(context, flightList.get(position), favFlightList, position);
    }

    @Override
    public int getItemCount() {
        return flightList.size();
    }

    public void updateFlightList(List<Flight> flightList) {
        //ToDO need to clear data
        this.flightList.clear();
        this.flightList.addAll(flightList);
        notifyDataSetChanged();
    }

    public void updateFavFlightList(List<FavouritesFlight> flightList) {
        //ToDO need to clear data
        this.favFlightList.clear();
        this.favFlightList.addAll(flightList);
//        notifyDataSetChanged();
    }

    class FlightInfoHolder extends RecyclerView.ViewHolder {
        ItemFlightBinding binding;
        FlightListActionListener listener;

        public FlightInfoHolder(@NonNull ItemFlightBinding binding, FlightListActionListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
        }

        public void bind(Context context, Flight flight, List<FavouritesFlight> favFlightList, int position) {
            binding.missionName.setText(flight.getMission_name());
            binding.launchDate.setText(flight.getLaunch_date_local());
            binding.rocketName.setText(flight.getRocket().getRocket_name());
            binding.status.setText(flight.getLaunchStatus());
            if (flight.getLinks() != null) {
                Glide.with(context)
                        .load(flight.getLinks().getMission_patch_small())
                        .placeholder(R.drawable.ic_flight) // Optional placeholder image while loading
                        .error(R.drawable.ic_flight) // Optional error image if the load fails
                        .into(binding.imageFlight);
            }

            for (FavouritesFlight flight1 : favFlightList) {
                if (flight1.getFlight_number() == flight.getFlight_number()) {
                    binding.favButton.setImageResource(R.drawable.ic_fav_selected);
                    return;
                } else {
                    binding.favButton.setImageResource(R.drawable.ic_fav_not_selected);
                }
            }

            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onFlightSelected(flight);
                }
            });

            binding.favButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.favButton.setImageResource(R.drawable.ic_fav_selected);
                    listener.onFavAdded(flight);
                    /*TODO
                     *   remove Fav logic need to add
                     *
                     * */
                }
            });

        }
    }

    public interface FlightListActionListener {
        public void onFavAdded(Flight flight);

        public void onFlightSelected(Flight flight);
    }
}
