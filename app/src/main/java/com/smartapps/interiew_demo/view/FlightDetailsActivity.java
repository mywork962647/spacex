package com.smartapps.interiew_demo.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.smartapps.interiew_demo.R;
import com.smartapps.interiew_demo.databinding.ActivityFlightDetailsBinding;
import com.smartapps.interiew_demo.model.Flight;
import com.smartapps.interiew_demo.util.LogUtils;
import com.smartapps.interiew_demo.util.ObjectConverter;

public class FlightDetailsActivity extends AppCompatActivity {
    private static final String TAG = FlightDetailsActivity.class.toString();
    private ActivityFlightDetailsBinding binding;
    private Flight selectedFlight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityFlightDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getSupportActionBar().setTitle("Flight details");

        String flightDetails = getIntent().getStringExtra("Flight_info");
        selectedFlight = ObjectConverter.toFlight(flightDetails);
        LogUtils.d(TAG, "onCreate: " + selectedFlight.toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (selectedFlight.getLinks() != null) {
            Glide.with(this)
                    .load(selectedFlight.getLinks().getMission_patch())
                    .placeholder(R.drawable.ic_flight) // Optional placeholder image while loading
                    .error(R.drawable.ic_flight) // Optional error image if the load fails
                    .into(binding.imageFlight);
        }

        binding.missionName.setText(selectedFlight.getMission_name());
        binding.missionDate.setText(selectedFlight.getLaunch_date_local());
        /* sec 2*/
        binding.rocketName.setText(selectedFlight.getRocket().getRocket_name());
        binding.rocketType.setText(selectedFlight.getRocket().getRocket_type());
        binding.rocketDetails.setText(selectedFlight.getDetails());

        /* sec 3*/
        binding.launchStatus.setText(selectedFlight.getLaunchStatus());
        binding.launchWindow.setText(""+selectedFlight.getLaunch_window());
        binding.launchSite.setText(selectedFlight.getLaunch_site().getSite_name());

        /* sec 4*/
        binding.link1.setText(selectedFlight.getLinks().getArticle_link());
        binding.link2.setText(selectedFlight.getLinks().getWikipedia());
        binding.link3.setText(selectedFlight.getLinks().getVideo_link());
    }
}