package com.smartapps.interiew_demo.view;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.smartapps.interiew_demo.R;
import com.smartapps.interiew_demo.databinding.ActivityHomeScreenBinding;
import com.smartapps.interiew_demo.model.Flight;
import com.smartapps.interiew_demo.util.LogUtils;
import com.smartapps.interiew_demo.util.ObjectConverter;
import com.smartapps.interiew_demo.view.adapter.FlightListAdapter;
import com.smartapps.interiew_demo.viewModel.FlightInfoViewModel;

public class HomeScreenActivity extends AppCompatActivity {
    private static final String TAG = HomeScreenActivity.class.getSimpleName();
    private ActivityHomeScreenBinding binding;
    private FlightInfoViewModel viewModel;
    private FlightListAdapter adapter;

    private boolean appCacheCleared = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeScreenBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        viewModel = new ViewModelProvider(this).get(FlightInfoViewModel.class);
        subscribeToDataChanges();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item1:
                startActivity(new Intent(HomeScreenActivity.this, FavouriteFlightListActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                return true;
            case R.id.menu_item2:
                appCacheCleared = true;
                viewModel.clearAppCache();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void subscribeToDataChanges() {
        /* Server API Response handled here  */
        viewModel.getResponseStatus().observe(this, responseStatus -> {
            switch (responseStatus) {
                case LOADING:
                    LogUtils.d(TAG, "getResponseStatus: LOADING");
                    //TODO Loading View need to handle here
                    break;
                case SUCCESS:
                    //TODO Loading View need to handle here
                    LogUtils.d(TAG, "getResponseStatus: SUCCESS");
                    break;
                case FAILED:
                    LogUtils.d(TAG, "getResponseStatus: FAILED");
                    Toast.makeText(this, "Loading failed. Please refresh the screen.", Toast.LENGTH_SHORT).show();
                    break;
            }
        });

        /* Fav Flights list handled here  */
        viewModel.getFavFlightList().observe(this, flights -> {
            adapter.updateFavFlightList(flights);
        });

        /* LocalCacheFlightList handled here  */
        viewModel.getLocalCacheFlightList().observe(this, flights -> {
            if (!flights.isEmpty()) {
                adapter.updateFlightList(flights);
            } else {
                if (appCacheCleared) {
                    appCacheCleared = false;
                    LogUtils.d(TAG, "Local Cache cleared :: " + flights.size());
                    Toast.makeText(this, "Cache cleared", Toast.LENGTH_SHORT).show();
                    return;
                }
                LogUtils.d(TAG, "subscribeDataEvents: Local Cache not found ");
                viewModel.fetchFlightInfo();
            }
        });

        /* Cloud response handled here  */
        viewModel.getFlightInfoList().observe(this, flights -> {
            if (binding.swipeRefreshLayout.isRefreshing()) {
                binding.swipeRefreshLayout.setRefreshing(false);
            }
            viewModel.cacheFlightData(flights);
        });
    }

    private void initView() {
        adapter = new FlightListAdapter(this, listener);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setAdapter(adapter);

        binding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                viewModel.fetchFlightInfo();
                binding.swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public FlightListAdapter.FlightListActionListener listener = new FlightListAdapter.FlightListActionListener() {
        @Override
        public void onFavAdded(Flight flight) {
            viewModel.addFavFlight(ObjectConverter.toFavFlight(flight));
        }

        @Override
        public void onFlightSelected(Flight flight) {
            Intent intent = new Intent(HomeScreenActivity.this, FlightDetailsActivity.class);
            intent.putExtra("Flight_info", ObjectConverter.toString(flight));
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    };
}