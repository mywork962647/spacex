package com.smartapps.interiew_demo.viewModel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;
import com.smartapps.interiew_demo.model.repository.AppRepository;
import com.smartapps.interiew_demo.model.network.ResponseStatus;

import java.util.List;

public class FlightInfoViewModel extends AndroidViewModel {
    private static final String TAG = FlightInfoViewModel.class.getSimpleName();
    private AppRepository appRepository;
    private LiveData<List<Flight>> flightInfoList;
    private LiveData<List<Flight>> localCacheFlightList;

    private LiveData<List<FavouritesFlight>> favFlightList;

    private LiveData<ResponseStatus> responseStatus;

    public FlightInfoViewModel(Application application) {
        super(application);
        /* LocalDB*/
        appRepository = AppRepository.getInstance(application);
        flightInfoList = appRepository.getFlightInfoList();
        localCacheFlightList = appRepository.getLocalCacheFlightList();
        favFlightList = appRepository.getAllFavFlights();
        responseStatus = appRepository.getResponseStatus();
    }

    public void cacheFlightData(List<Flight> flights) {
        appRepository.addFlights(flights);
    }

    public void clearAppCache() {
        appRepository.clearLocalCache();
    }

    public LiveData<List<Flight>> getFlightInfoList() {
        return flightInfoList;
    }

    public LiveData<List<Flight>> getLocalCacheFlightList() {
        return localCacheFlightList;
    }

    public LiveData<List<FavouritesFlight>> getFavFlightList() {
        return favFlightList;
    }

    public LiveData<ResponseStatus> getResponseStatus() {
        return responseStatus;
    }

    public void addFavFlight(FavouritesFlight flight) {
        appRepository.addFavFlight(flight);
    }

    public void fetchFlightInfo() {
        appRepository.fetchFightDetailsFromCloud();
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        appRepository.clearSubscriptions();
    }
}
