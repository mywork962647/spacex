package com.smartapps.interiew_demo.util;

import com.google.gson.Gson;
import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;

public class ObjectConverter {
    public static FavouritesFlight toFavFlight(Flight json) {
        String flight = new Gson().toJson(json);
        return new Gson().fromJson(flight, FavouritesFlight.class);
    }

    public static String toString(Flight json) {
        return new Gson().toJson(json);
    }

    public static Flight toFlight(String json) {
        return new Gson().fromJson(json, Flight.class);
    }
}
