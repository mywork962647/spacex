package com.smartapps.interiew_demo.util;

import android.util.Log;

public class LogUtils {
    private static final String TAG = "APP:SpaceX";

    public static void d(String tag, String message) {
        Log.d(TAG, tag + " : " + message);
    }

    public static void d(String message) {
        Log.d(TAG, " " + message);
    }

}
