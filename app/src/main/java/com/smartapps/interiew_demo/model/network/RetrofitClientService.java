package com.smartapps.interiew_demo.model.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientService {

    private static String BASE_URL = "https://api.spacexdata.com/";

    private static FlightApi flightApi;

    public static FlightApi getInstance() {
        if (flightApi == null) {
            flightApi = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build()
                    .create(FlightApi.class);
        }
        return flightApi;
    }
}
