package com.smartapps.interiew_demo.model.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;
import com.smartapps.interiew_demo.model.network.FlightApi;
import com.smartapps.interiew_demo.model.network.ResponseStatus;
import com.smartapps.interiew_demo.model.network.RetrofitClientService;
import com.smartapps.interiew_demo.model.roomDB.dao.FavFlightDao;
import com.smartapps.interiew_demo.model.roomDB.dao.FlightDao;
import com.smartapps.interiew_demo.model.roomDB.MyRoomDB;
import com.smartapps.interiew_demo.util.LogUtils;


import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AppRepository {
    private static final String TAG = "AppRepository";
    private MutableLiveData<List<Flight>> flightInfoList = new MutableLiveData<>();
    private MutableLiveData<ResponseStatus> responseStatus = new MutableLiveData<>();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private static AppRepository appRepository;
    private FlightApi flightApi;

    private FlightDao flightDao;
    private FavFlightDao favFlightDao;

    private AppRepository(Context applicationContext) {
        flightApi = RetrofitClientService.getInstance();
        flightDao = MyRoomDB.getInstance(applicationContext).flightDao();
        favFlightDao = MyRoomDB.getInstance(applicationContext).favouritesFlightDao();
    }

    public static AppRepository getInstance(Context applicationContext) {
        if (appRepository == null) {
            appRepository = new AppRepository(applicationContext);
        }
        return appRepository;
    }

    public LiveData<List<Flight>> getFlightInfoList() {
        return flightInfoList;
    }

    public LiveData<ResponseStatus> getResponseStatus() {
        return responseStatus;
    }

    /*   ----------------------  Local DB ------------------------*/

    public LiveData<List<Flight>> getLocalCacheFlightList() {
        return flightDao.getAllFlights();
    }

    public void addFlight(Flight flight) {
        flightDao.addFlight(flight);
    }

    public void addFlights(List<Flight> flights) {
        flightDao.addFlights(flights);
    }

    public void clearLocalCache() {
        flightDao.deleteAll();
    }


    /* Fav List */

    public LiveData<List<FavouritesFlight>> getAllFavFlights() {
        return favFlightDao.getAllFavFlights();
    }

    public void addFavFlight(FavouritesFlight flight) {
        favFlightDao.addFavFlight(flight);
    }

    /* --------  Cloud ---------- */

    public void fetchFightDetailsFromCloud() {
        responseStatus.setValue(ResponseStatus.LOADING);
        Single<List<Flight>> flightCall = flightApi.getFlightList();
        compositeDisposable.add(flightCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Flight>>() {
                    @Override
                    public void onSuccess(@NonNull List<Flight> flights) {
                        LogUtils.d(TAG, "onSuccess: " + flights.size());
                        responseStatus.setValue(ResponseStatus.SUCCESS);
                        flightInfoList.setValue(flights);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        //TODO
                        responseStatus.setValue(ResponseStatus.FAILED);
                    }
                }));
    }

    /*-------------------------------*/

    public void clearSubscriptions() {
        compositeDisposable.clear();
    }
}
