package com.smartapps.interiew_demo.model.network;

import com.smartapps.interiew_demo.model.Flight;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;

public interface FlightApi {
    @GET("/v3/launches")
    public Single<List<Flight>> getFlightList();
}
