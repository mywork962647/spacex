package com.smartapps.interiew_demo.model.roomDB;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.smartapps.interiew_demo.model.FavouritesFlight;
import com.smartapps.interiew_demo.model.Flight;
import com.smartapps.interiew_demo.model.roomDB.dao.FavFlightDao;
import com.smartapps.interiew_demo.model.roomDB.dao.FlightDao;

@Database(entities = {Flight.class, FavouritesFlight.class}, exportSchema = false, version = 1)
@TypeConverters({Converters.class})
public abstract class MyRoomDB extends RoomDatabase {
    public static String DB_NAME = "SpaceXDB";
    public static MyRoomDB myRoomDb;

    public static synchronized MyRoomDB getInstance(Context context) {
        if (myRoomDb == null) {
            myRoomDb = Room.databaseBuilder(context.getApplicationContext(), MyRoomDB.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return myRoomDb;
    }

    public abstract FlightDao flightDao();

    public abstract FavFlightDao favouritesFlightDao();
}
