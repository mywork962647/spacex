package com.smartapps.interiew_demo.model;

public class Links {
    private String mission_patch;
    private String mission_patch_small;
    private String article_link;
    private String wikipedia;
    private String video_link;
    private String youtube_id;

    public Links() {
    }

    public Links(String mission_patch, String mission_patch_small, String article_link, String wikipedia, String video_link, String youtube_id) {
        this.mission_patch = mission_patch;
        this.mission_patch_small = mission_patch_small;
        this.article_link = article_link;
        this.wikipedia = wikipedia;
        this.video_link = video_link;
        this.youtube_id = youtube_id;
    }

    public String getMission_patch() {
        if (mission_patch == null)
            return "";
        return mission_patch;
    }

    public void setMission_patch(String mission_patch) {
        this.mission_patch = mission_patch;
    }

    public String getMission_patch_small() {
        return mission_patch_small;
    }

    public void setMission_patch_small(String mission_patch_small) {
        this.mission_patch_small = mission_patch_small;
    }

    public String getArticle_link() {
        if (article_link == null)
            return "";
        return article_link;
    }

    public void setArticle_link(String article_link) {
        this.article_link = article_link;
    }

    public String getWikipedia() {
        if (wikipedia == null)
            return "";
        return wikipedia;
    }

    public void setWikipedia(String wikipedia) {
        this.wikipedia = wikipedia;
    }

    public String getVideo_link() {
        if (video_link == null)
            return "";
        return video_link;
    }

    public void setVideo_link(String video_link) {
        this.video_link = video_link;
    }

    public String getYoutube_id() {
        return youtube_id;
    }

    public void setYoutube_id(String youtube_id) {
        this.youtube_id = youtube_id;
    }

    @Override
    public String toString() {
        return "Links{" +
                "mission_patch='" + mission_patch + '\'' +
                ", mission_patch_small='" + mission_patch_small + '\'' +
                ", article_link='" + article_link + '\'' +
                ", wikipedia='" + wikipedia + '\'' +
                ", video_link='" + video_link + '\'' +
                ", youtube_id='" + youtube_id + '\'' +
                '}';
    }
}
