package com.smartapps.interiew_demo.model.network;

public enum ResponseStatus {
    LOADING, SUCCESS, FAILED
}
