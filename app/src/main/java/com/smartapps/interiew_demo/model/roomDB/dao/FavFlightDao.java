package com.smartapps.interiew_demo.model.roomDB.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.smartapps.interiew_demo.model.FavouritesFlight;

import java.util.List;

@Dao
public interface FavFlightDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addFavFlight(FavouritesFlight Flight);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addFavFlights(List<FavouritesFlight> Flight);

    @Query("Select * from Fav_Flight_Table")
    public LiveData<List<FavouritesFlight>> getAllFavFlights();

    @Query("DELETE from Fav_Flight_Table")
    public void deleteAll();
}
