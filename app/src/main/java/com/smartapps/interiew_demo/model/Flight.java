package com.smartapps.interiew_demo.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.smartapps.interiew_demo.model.roomDB.Converters;

@Entity(tableName = "Flight_Table")
@TypeConverters(Converters.class)
public class Flight {

    @PrimaryKey(autoGenerate = true)
    private int flight_number;
    private String mission_name;
    private String launch_date_local;
    @Embedded
    private Rocket rocket;
    @Embedded
    private Links links;
    @Embedded
    private LaunchSite launch_site;
    private boolean launch_success;

    private String details;

    private int launch_window;

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String getLaunch_date_local() {
        return launch_date_local;
    }

    public void setLaunch_date_local(String launch_year) {
        this.launch_date_local = launch_year;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public void setRocket(Rocket rocket) {
        this.rocket = rocket;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public LaunchSite getLaunch_site() {
        return launch_site;
    }

    public void setLaunch_site(LaunchSite launch_site) {
        this.launch_site = launch_site;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public boolean isLaunch_success() {
        return launch_success;
    }

    public String getLaunchStatus() {
        if (launch_success) {
            return "Launched";
        } else
            return "Launch Failed";
    }

    public void setLaunch_success(boolean launch_success) {
        this.launch_success = launch_success;
    }

    public int getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(int flight_number) {
        this.flight_number = flight_number;
    }

    public int getLaunch_window() {
        return launch_window;
    }

    public void setLaunch_window(int launch_window) {
        this.launch_window = launch_window;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flight_number=" + flight_number +
                ", mission_name='" + mission_name + '\'' +
                ", launch_date_local='" + launch_date_local + '\'' +
                ", rocket=" + rocket.toString() +
                ", links=" + links.toString() +
                ", launch_site=" + launch_site.toString() +
                ", launch_success=" + launch_success +
                ", details='" + details + '\'' +
                ", launch_window=" + launch_window +
                '}';
    }
}
