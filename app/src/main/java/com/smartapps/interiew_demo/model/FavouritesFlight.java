package com.smartapps.interiew_demo.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.smartapps.interiew_demo.model.roomDB.Converters;

@Entity(tableName = "Fav_Flight_Table")
@TypeConverters(Converters.class)
public class FavouritesFlight {
    @PrimaryKey()
    private int flight_number;
    private String mission_name;
    private String launch_date_local;
    @Embedded
    private Rocket rocket;
    @Embedded
    private Links links;
    private boolean launch_success;

    public FavouritesFlight(String mission_name, String launch_date_local, Rocket rocket, Links links, boolean launch_success) {
        this.mission_name = mission_name;
        this.launch_date_local = launch_date_local;
        this.rocket = rocket;
        this.links = links;
        this.launch_success = launch_success;
    }

    public String getMission_name() {
        return mission_name;
    }

    public void setMission_name(String mission_name) {
        this.mission_name = mission_name;
    }

    public String getLaunch_date_local() {
        return launch_date_local;
    }

    public void setLaunch_date_local(String launch_year) {
        this.launch_date_local = launch_year;
    }

    public Rocket getRocket() {
        return rocket;
    }

    public void setRocket(Rocket rocket) {
        this.rocket = rocket;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public boolean isLaunch_success() {
        return launch_success;
    }

    public String getLaunchStatus() {
        if (launch_success) {
            return "Launched";
        } else
            return "Launch Failed";
    }

    public void setLaunch_success(boolean launch_success) {
        this.launch_success = launch_success;
    }

    public int getFlight_number() {
        return flight_number;
    }

    public void setFlight_number(int flight_number) {
        this.flight_number = flight_number;
    }

    @Override
    public String toString() {
        return "FavouritesFlight{" +
                "mission_name='" + mission_name + '\'' +
                ", launch_year='" + launch_date_local + '\'' +
                ", rocket=" + rocket.toString() +
                ", links=" + links.toString() +
                ", launch_success=" + launch_success +
                ", flight_number=" + flight_number +
                '}';
    }
}
