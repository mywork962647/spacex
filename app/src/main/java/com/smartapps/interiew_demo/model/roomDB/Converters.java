package com.smartapps.interiew_demo.model.roomDB;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.smartapps.interiew_demo.model.Flight;

public class Converters {
    @TypeConverter
    public static String fromComplexObject(Flight complexObject) {
        return new Gson().toJson(complexObject);
    }

    @TypeConverter
    public static Flight toComplexObject(String json) {
        return new Gson().fromJson(json, Flight.class);
    }
}
