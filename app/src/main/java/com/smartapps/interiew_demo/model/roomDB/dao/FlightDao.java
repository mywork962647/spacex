package com.smartapps.interiew_demo.model.roomDB.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.smartapps.interiew_demo.model.Flight;

import java.util.List;

@Dao
public interface FlightDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addFlight(Flight Flight);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void addFlights(List<Flight> Flight);

    @Query("Select * from Flight_Table")
    public LiveData<List<Flight>> getAllFlights();

    @Query("DELETE from Flight_Table")
    public void deleteAll();
}
